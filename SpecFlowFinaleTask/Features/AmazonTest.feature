﻿Feature: AmazonSite

@mytag
Scenario: User should be able to authorize with correct credentials
	Given User open Home page
	When User click on Sign In button
	Then User should be navigated to authorization page
	Given User see Authorization page1
	When User enter login v-i-tt-a@mail.ru and click Continue button
	Then User should be navigated to next authorization page 
	Given User see Authorization page2
	When User enter password baci1us_anthraX and click SignIn button
	Then User see message Important Message!
	
Scenario: User should not be able to authorize with incorrect credentials
	Given User open Home page
	When User click on Sign In button
	Then User should be navigated to authorization page
	Given User see Authorization page1
	When User enter login random@mail.ru and click Continue button
	Then User should be navigated to authorization page next
	And User should observe message Password reset required

Scenario: User wants to change country or region
    Given User open Home page
	When User click on button with country flag picture
	Then User should be navigated to Language Settings page
	Given User see Language Settings page
	When User click on Deutsch button
	Then User see text Wähle die Währung aus, in der du einkaufen möchtest.

Scenario: The ability for the user to use product categories from a drop-down list
	Given User open Home page
	When User click on All button
	Then User see a drop-down list and click Amazon Music button
	And User can remove the dropdown list
	And  User see Home Page

Scenario: User selects a product from the list
    Given User open Home page 
	When User click on All button
	And User click on Computers button and select Computer Components button
	Then User should be navigated to Search Products page
	And User click on image product SAMSUNG 970 and see Product page
	
Scenario: User selects a product and adds it to the waiting list
    Given User open Home page 
	When User click on All button
	And User click on Computers button and select Computer Components button
	Then User should be navigated to Search Products page
	And User click on image product SAMSUNG 970 and see Product page
	And User adds product to the waiting list
	And User should be navigated to authorization page

Scenario: User looking for a product in the search bar
    Given  User open Home page
	When User click on Search button and writes the product name SAMSUNG 970
	And  User click on Search button
	Then User should be navigated to Result page
	
Scenario: User to be able to select a product from the homepage
	Given User open Home page
	When User click on Computer Mice Image area
	Then User see page with selected products

Scenario: User filters products by brand
    Given User open Home page
	When User click on Computer Mice Image area
	Then User see page with selected products
	Given User click Asus button in filtr
	When User should be navigated to NewSelectSearch page
	Then User see selected product by brand

Scenario: User input name product Cyrillic
    Given User open Home page
	When User click on Search button and writes the product name лпрптчмии
	And  User click on Search button
	Then User should be navigated to No Result page and see message No result