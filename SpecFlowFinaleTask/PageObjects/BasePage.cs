﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace SpecFlowFinaleTask.PageObjects
{
    public class BasePage
    {
        protected IWebDriver WebDriver { get; set; }
        public BasePage(IWebDriver webDriver)
        {
            if (webDriver == null)
            {
                throw new ArgumentNullException(nameof(webDriver));
            }
           
            WebDriver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }
    }
}
