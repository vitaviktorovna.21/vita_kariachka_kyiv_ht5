﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class NoResultPageObject : BasePage
    {
        public NoResultPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='search']")]
        public IWebElement NoResultText { get; set; }
    }
}
