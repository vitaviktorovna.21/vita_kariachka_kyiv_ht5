﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class HomePageObject : BasePage
    {
        public HomePageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='nav-link-accountList-nav-line-1']")]
        public IWebElement SignInButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='auth-warning-message-box']")]
        public IWebElement Captcha { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='icp-nav-flyout']")]
        public IWebElement FlagPictureButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='nav-hamburger-menu']")]
        public IWebElement AllButton { get; set; }
       
        [FindsBy(How = How.XPath, Using = "//*[@id='hmenu-content']/ul[1]/li[2]/a")]
        public IWebElement AmazonMusicButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='hmenu-canvas-background']/div")]
        public IWebElement CloseDropDownButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='hmenu-content']/ul[1]/li[8]/a")]
        public IWebElement ComputerButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='hmenu-content']/ul[6]/li[4]/a")]
        public IWebElement ComputerComponentButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='twotabsearchtextbox']")]
        public IWebElement SearchInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='nav-search-submit-button']")]
        public IWebElement SearchButton { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//span[contains(.,'Computer mice')]")]
        public IWebElement ProductHomePageButton { get; set; }
    }
}
