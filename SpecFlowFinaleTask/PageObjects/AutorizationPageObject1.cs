﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects 
{
    public class AutorizationPageObject1 : BasePage
    {
        public AutorizationPageObject1(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='ap_email']")]
        public IWebElement LoginInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@id='continue']")]
        public IWebElement ContinueButton { get; set; }

        
    }
}
