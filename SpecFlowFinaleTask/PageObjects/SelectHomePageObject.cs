﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class SelectHomePageObject : BasePage
    {
        public SelectHomePageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='p_89/Asus']/span/a")]
        public IWebElement AsusBrandButton { get; set; }
    }
}
