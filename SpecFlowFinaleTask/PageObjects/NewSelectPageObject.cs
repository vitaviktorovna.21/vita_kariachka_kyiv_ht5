﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class NewSelectPageObject : BasePage
    {
        public NewSelectPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//div[@class='s-card-container s-overflow-hidden aok-relative s-include-content-margin s-latency-cf-section s-card-border']")]
        public IList<IWebElement> NewResultSearchField { get; set; }
    }
}
