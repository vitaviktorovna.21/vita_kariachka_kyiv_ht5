﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class ProductUsersPageObject : BasePage
    {
        public ProductUsersPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='productTitle']")]
        public IWebElement ProductDescriptionText { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='wishListMainButton']/span/a")]
        public IWebElement AddToListButton { get; set; }
    }
}
