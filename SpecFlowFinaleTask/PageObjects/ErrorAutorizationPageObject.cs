﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class ErrorAutorizationPageObject : BasePage
    {
        public ErrorAutorizationPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='authportal-main-section']")]
        public IWebElement ErrorMessage { get; set; }
    }
}
