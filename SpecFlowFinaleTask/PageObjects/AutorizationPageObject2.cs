﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class AutorizationPageObject2 : BasePage
    {
        public AutorizationPageObject2(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='ap_password']")]
        public IWebElement PasswordInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='signInSubmit']")]
        public IWebElement EnterButton { get; set; }
    }
}
