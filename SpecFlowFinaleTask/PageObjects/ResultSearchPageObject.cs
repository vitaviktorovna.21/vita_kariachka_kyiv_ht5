﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class ResultSearchPageObject : BasePage
    {
        public ResultSearchPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//div[@class='s-no-outline']//span[@class='a-size-medium a-color-base a-text-normal']")]
        public IWebElement ResultButton { get; set; }
    }
}
