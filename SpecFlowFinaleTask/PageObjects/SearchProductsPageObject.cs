﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class SearchProductsPageObject : BasePage
    {
        public SearchProductsPageObject(IWebDriver webDriver) : base(webDriver) { }
        

        [FindsBy(How = How.XPath, Using = "//*[@id='search']/div[1]/div[1]/div/span[3]/div[2]/div[3]")]
        public IWebElement PictureProductPlace { get; set; }
    }
}
