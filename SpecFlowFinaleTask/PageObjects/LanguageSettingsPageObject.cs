﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.PageObjects
{
    public class LanguageSettingsPageObject : BasePage
    {
        public LanguageSettingsPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='icp-language-settings']/div[5]/div/label")]
        public IWebElement DeutschLanguageButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='icp-currency-subheading']")]
        public IWebElement DeutschLanguageMessage { get; set; }
    }
}
