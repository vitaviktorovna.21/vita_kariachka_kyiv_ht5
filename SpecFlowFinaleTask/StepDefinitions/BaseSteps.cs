﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecFlowFinaleTask.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowFinaleTask.StepDefinitions
{
    public class BaseSteps
    {
        protected IWebDriver _webdriver;

        [AfterScenario]
        protected void DoAfterEach()
        {
            _webdriver.Quit();
        }

        [BeforeScenario]
        protected void DoBeforeEach()
        {
            _webdriver = new ChromeDriver();
            _webdriver.Manage().Cookies.DeleteAllCookies();
            _webdriver.Navigate().GoToUrl(TestSettings.HostPrefix);
            _webdriver.Manage().Window.Maximize();
        }
    }
}
