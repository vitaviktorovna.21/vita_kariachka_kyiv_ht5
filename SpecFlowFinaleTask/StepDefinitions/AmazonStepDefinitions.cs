﻿using SpecFlowFinaleTask.PageObjects;

namespace SpecFlowFinaleTask.StepDefinitions
{
    [Binding]
    public sealed class AmazonTestStepDefinitions : BaseSteps
    {
        private HomePageObject homePage;
        private AutorizationPageObject1 autorizationPage1;
        private AutorizationPageObject2 autorizationPage2;
        private ErrorAutorizationPageObject errorAutorization;
        private LanguageSettingsPageObject languageSettingsPage;
        private SearchProductsPageObject searchProductsPage;
        private ProductUsersPageObject productUsersPage;
        private ResultSearchPageObject resultSearchPage;
        private SelectHomePageObject selectHomePage;
        private NewSelectPageObject newSelectPage;
        private NoResultPageObject noResultPage;
        
        [Given(@"User open Home page")]
        public void GivenUserOpenHomePage()
        {   
            homePage = new HomePageObject(_webdriver);
        }

        [Given(@"User see Authorization page1")]
        public void GivenUserSeeAuthorizationPage1()
        {
            autorizationPage1 = new AutorizationPageObject1(_webdriver);
        }

        [Given(@"User see Authorization page2")]
        public void GivenUserSeeAuthorizationPage2()
        {
            autorizationPage2 = new AutorizationPageObject2(_webdriver);
        }

        [Given(@"User see Language Settings page")]
        public void GivenUserSeeLanguageSettingsPage()
        {
            languageSettingsPage = new LanguageSettingsPageObject(_webdriver);
        }

        [Given(@"User click Asus button in filtr")]
        public void GivenUserClickAsusButtonInFiltr()
        {
            selectHomePage.AsusBrandButton.Click();
        }

        [When(@"User click on Computers button and select Computer Components button")]
        public void WhenUserClickOnComputersButtonAndSelectComputerComponentsButton()
        {
            homePage.ComputerButton.Click();
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            homePage.ComputerComponentButton.Click();
        }

        [When(@"User click on Sign In button")]
        public void WhenUserClickOnSignInButton()
        {
            homePage.SignInButton.Click();
        }

        [When(@"User enter login (.*) and click Continue button")]
        public void WhenUserEnterCorrectLoginAndClickContinueButton(string login)
        {
            autorizationPage1.LoginInput.SendKeys(login);
            autorizationPage1.ContinueButton.Click();
        }

        [When(@"User enter password (.*) and click SignIn button")]
        public void WhenUserEnterCorrectPasswordAndClickSign_InButton(string password)
        {
            autorizationPage2.PasswordInput.SendKeys(password);
            autorizationPage2.EnterButton.Click();
        }

        [When(@"User click on button with country flag picture")]
        public void WhenUserClickOnButtonWithCountryFlagPicture()
        {
            homePage.FlagPictureButton.Click();
        }

        [When(@"User click on Deutsch button")]
        public void WhenUserClickOnDeutschButton()
        {
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            languageSettingsPage.DeutschLanguageButton.Click();
        }

        [When(@"User click on All button")]
        public void WhenUserClickOnAllButton()
        {
            homePage.AllButton.Click();
        }

        [When(@"User click on Search button and writes the product name (.*)")]
        public void WhenUserClickOnSearchButtonAndWritesTheProductNameSAMSUNG(string message)
        {
            homePage.SearchInput.SendKeys(message);
        }

        [When(@"User click on Search button")]
        public void WhenUserClickOnSearchButton()
        {
            homePage.SearchButton.Click();
        }

        [When(@"User click on Computer Mice Image area")]
        public void WhenUserClickOnComputerMiceImageArea()
        {
            homePage.ProductHomePageButton.Click();
        }

        [When(@"User should be navigated to NewSelectSearch page")]
        public void WhenUserShouldBeNavigatedToNewSelectSearchPage()
        {
            _webdriver.Url.Should().Contain("Asus");
        }

        [Then(@"User should be navigated to authorization page")]
        public void ThenUserShouldBeNavigatedToAuthoriztionPage()
        {
            _webdriver.Url.Should().Contain("openid.return_to");
        }

        [Then(@"User should be navigated to next authorization page")]
        public void ThenUserShouldBeNavigatedToNextAuthoriztionPage()
        {
            _webdriver.Url.Should().Contain("amazon.com/ap/signin");
        }

        [Then(@"User should be navigated to authorization page next")]
        public void ThenUserShouldBeNavigatedToAuthoriztionPageNext()
        {
            _webdriver.Url.Should().Contain("forgotpassword");
        }

        [Then(@"User see message (.*)")]
        public void ThenUserCaptchaPage(string message)
        {
            homePage.Captcha.Text.Should().Contain(message);
        }

        [Then(@"User should observe message (.*)")]
        public void ThenUserShouldObserveMessageThereWasAProblem(string message)
        {
            errorAutorization = new ErrorAutorizationPageObject(_webdriver);
            errorAutorization.ErrorMessage.Text.Should().Contain(message);
        }

        [Then(@"User should be navigated to Language Settings page")]
        public void ThenUserShouldBeNavigatedToLanguageSettingsPage()
        {
            _webdriver.Url.Should().Contain("customer-preferences/edit");
        }
        [Then(@"User see text (.*)")]
        public void ThenUserSeeTextWahleDieWahrungAusInDerDuEinkaufenMochtest_(string message)
        {
            languageSettingsPage.DeutschLanguageMessage.Text.Should().Contain(message);
        }

        [Then(@"User see a drop-down list and click Amazon Music button")]
        public void ThenUserSeeADrop_DownListAndClickAmazonMusicButton()
        {
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            homePage.AmazonMusicButton.Click();
        }

        [Then(@"User can remove the dropdown list")]
        public void ThenUserCanRemoveTheDropdownList()
        {
            homePage.CloseDropDownButton.Click();
        }

        [Then(@"User see Home Page")]
        public void ThenUserSeeHomePage()
        {
            _webdriver.Url.Should().Contain("amazon.com");
        }

        [Then(@"User should be navigated to Search Products page")]
        public void ThenUserShouldBeNavigatedToSearchProductsPage()
        {
            searchProductsPage = new SearchProductsPageObject(_webdriver);
            searchProductsPage.PictureProductPlace.Click();
        }

        [Then(@"User click on image product (.*) and see Product page")]
        public void ThenUserClickOnImageProductSAMSUNGAndSeeProductPage(string message)
        {
            productUsersPage = new ProductUsersPageObject(_webdriver);
            productUsersPage.ProductDescriptionText.Text.Should().Contain(message);
        }

        [Then(@"User adds product to the waiting list")]
        public void ThenUserAddsProductToTheWaitingList()
        {
            productUsersPage.AddToListButton.Click();
        }

        [Then(@"User should be navigated to Result page")]
        public void ThenUserShouldBeNavigatedToResultPage()
        {
            resultSearchPage = new ResultSearchPageObject(_webdriver);
            _webdriver.Url.Should().Contain("SAMSUNG");
        }

        [Then(@"User see page with selected products")]
        public void ThenUserSeePageWithSelectedProducts()
        {
            _webdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            selectHomePage = new SelectHomePageObject(_webdriver);
            _webdriver.Url.Should().Contain("gaming+mouse");
        }
        [Then(@"User see selected product by brand")]
        public void ThenUserSeeSelectedProductByBrand()
        {
            newSelectPage = new NewSelectPageObject(_webdriver);
            foreach (var field in newSelectPage.NewResultSearchField)
            {
                field.Text.ToLower().Should().Contain("asus");
            }
        }

        [Then(@"User should be navigated to No Result page and see message No result")]
        public void ThenUserShouldBeNavigatedToNoResultPageAndSeeMessageNoResult()
        {
            noResultPage = new NoResultPageObject(_webdriver);
            noResultPage.NoResultText.Text.Should().Contain("No result");
        }
    }
}